#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "boost/thread.hpp"
#include "mavlink.h"

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <time.h>
#include <arpa/inet.h>
#include <stdbool.h> /* required for the definition of bool in C99 */

#define BUFFER_LENGTH 2041 // minimum buffer size that can be used with qnx (I don't know why)

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow) {
    ui->setupUi(this);
    ui->button1->setProperty("pagematches", true);
    run();
}


MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::get_state() {
    while (1) {
        sleep(1);
        cout << "get_state!" << endl;
    }
}
uint64_t microsSinceEpoch() {

    struct timeval tv;

    uint64_t micros = 0;

    gettimeofday(&tv, NULL);
    micros =  ((uint64_t)tv.tv_sec) * 1000000 + tv.tv_usec;

    return micros;
}

void MainWindow::receive_data() {

    char target_ip[100];
    float position[6] = {};
    int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    struct sockaddr_in gcAddr;
    struct sockaddr_in locAddr;
    //struct sockaddr_in fromAddr;
    uint8_t buf[BUFFER_LENGTH];
    ssize_t recsize;
    socklen_t fromlen;
    int bytes_sent;
    mavlink_message_t msg;
    uint16_t len;
    int i = 0;
    //int success = 0;
    unsigned int temp = 0;


    // Change the target ip if parameter was given
    strcpy(target_ip, "loalhost");
    memset(&locAddr, 0, sizeof(locAddr));
    locAddr.sin_family = AF_INET;
    locAddr.sin_addr.s_addr = INADDR_ANY;
    locAddr.sin_port = htons(8000);




    /* Bind the socket to port 14551 - necessary to receive packets from qgroundcontrol */
    /*
    if (-1 == bind(sock,(struct sockaddr *)&locAddr, sizeof(struct sockaddr)))
    {
        perror("error bind failed");
       // close(sock);
        exit(EXIT_FAILURE);
    }
    */

    if (fcntl(sock, F_SETFL, O_NONBLOCK | O_ASYNC) < 0) {
        fprintf(stderr, "error setting nonblocking: %s\n", strerror(errno));
       // close(sock);
        exit(EXIT_FAILURE);
    }


    memset(&gcAddr, 0, sizeof(gcAddr));
    gcAddr.sin_family = AF_INET;
    gcAddr.sin_addr.s_addr = inet_addr(target_ip);
    gcAddr.sin_port = htons(8000);



    for (;;) {

        /*Send Heartbeat
        mavlink_msg_heartbeat_pack(1, 200, &msg, MAV_TYPE_HELICOPTER, MAV_AUTOPILOT_GENERIC, MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        bytes_sent = sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
        */

        /* Send Status
        mavlink_msg_sys_status_pack(1, 200, &msg, 0, 0, 0, 500, 11000, -1, -1, 0, 0, 0, 0, 0, 0);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        bytes_sent = sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof (struct sockaddr_in));
        */

        /* Send Local Position
        mavlink_msg_local_position_ned_pack(1, 200, &msg, microsSinceEpoch(),
                                        position[0], position[1], position[2],
                                        position[3], position[4], position[5]);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        bytes_sent = sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
        */

        /* Send attitude
        mavlink_msg_attitude_pack(1, 200, &msg, microsSinceEpoch(), 1.2, 1.7, 3.14, 0.01, 0.02, 0.03);
        len = mavlink_msg_to_send_buffer(buf, &msg);
        bytes_sent = sendto(sock, buf, len, 0, (struct sockaddr*)&gcAddr, sizeof(struct sockaddr_in));
        */

        memset(buf, 0, BUFFER_LENGTH);
        recsize = recvfrom(sock, (void *)buf, BUFFER_LENGTH, 0, (struct sockaddr *)&gcAddr, &fromlen);
        cout << "recsize: " << recsize << endl;
        /*
        if (recsize > 0)
        {
            // Something received - print out all bytes and parse packet
            mavlink_message_t msg;
            mavlink_status_t status;

            printf("Bytes Received: %d\nDatagram: ", (int)recsize);
            for (i = 0; i < recsize; ++i)
            {
                temp = buf[i];
                printf("%02x ", (unsigned char)temp);
                if (mavlink_parse_char(MAVLINK_COMM_0, buf[i], &msg, &status))
                {
                    // Packet received
                    printf("\nReceived packet: SYS: %d, COMP: %d, LEN: %d, MSG ID: %d\n", msg.sysid, msg.compid, msg.len, msg.msgid);
                }
            }
            printf("\n");

        }
        */
        memset(buf, 0, BUFFER_LENGTH);
        sleep(1); // Sleep one second
    }
}



void MainWindow::run() {



    boost::thread get_state_t( &MainWindow::get_state, this );
    boost::thread receive_data_t( &MainWindow::receive_data, this );

}

void MainWindow::on_tabWidget_currentChanged(int index)
{
    ui->button1->setProperty("pagematches", false);
    ui->button2->setProperty("pagematches", false);
    ui->button3->setProperty("pagematches", false);
    // Set one of the buttons to true
    if (index == 0)
    ui->button1->setProperty("pagematches", true);
    else if (index == 1)
    ui->button2->setProperty("pagematches", true);
    else
    ui->button3->setProperty("pagematches", true);
    // Update buttons style
    ui->button1->style()->polish(ui->button1);
    ui->button2->style()->polish(ui->button2);
    ui->button3->style()->polish(ui->button3);
}
